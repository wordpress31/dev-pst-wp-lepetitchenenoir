<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'lepetiaicm2020' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YG(Y&D42qQLSR02j:Gs3&$s {#JwNHl_AV8p&mB^SIwNV,IFxgri~P>Vd&1tG]yu' );
define( 'SECURE_AUTH_KEY',  'EopFxVX`u<e+ySVgEdg5qkAMCI63JVw/H}Ggh9P$xF)zc1)nh(Huu&2k><_O;.4R' );
define( 'LOGGED_IN_KEY',    'uFZRWJ#pOW2B&Bys+Q>o9+2iUN1c2v6FyBh7(LZ0`b4ZK{=E`<Y4P&BG,_Qktou3' );
define( 'NONCE_KEY',        '7Y4LaD0CeRE1T5/}Yq2Fno: @HA{e zvd-lu&+LEyh<T#Oua,(]FpI|.3_GQW2Kf' );
define( 'AUTH_SALT',        'qx;sx.1o (oHtL*eD|>lGO,~=Uq:SjsBog01JBCw.ag).DCFVoq&AGWXhyENew&D' );
define( 'SECURE_AUTH_SALT', 'dgV<nE3q!@OeHpdhR1pJSzGSYMhS{x[^Rc3joUT9 V5!!1Zk$P[6*Kxen Md@F64' );
define( 'LOGGED_IN_SALT',   '*N1z]TkK`^(SBfl_P&8Th?.{/_S/0hHrDM>IOKSt/@C9_SeRmsh&ox*3~#%*LU1W' );
define( 'NONCE_SALT',       '<J%pyF |N,0L:|uIk>,QEC@#*5l|Y,n_vw-2d3VuI7(WCZ2dL~pN^]H>;uKgkj4-' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'lp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
