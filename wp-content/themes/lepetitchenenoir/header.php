<!DOCTYPE html>
<html class=""<?php language_attributes(); ?>>
<head>
    <title><?php wp_title('|',true,'right'); bloginfo('name');?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/slick.css" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/slick-theme.css" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/responsive.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" />
    <link rel="icon" href="<?php bloginfo( 'template_directory' ); ?>/favicon.ico" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179546925-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-179546925-1');
        </script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="wrapper">
    <header class="header sticky-top" id="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-nav-contact">
            <div class="container">
                <div class="contact-header ml-auto">
                    <ul class="info-head">
                        <li>
                            <i class="far fa-envelope"></i>
                            <a href="mailto:<?php echo get_option('emailsite');?>"><?php echo get_option('emailsite');?></a>
                        </li>
                        <li>
                            <i class="fas fa-phone"></i>
                            <a href="tel:<?php echo get_option('telephonesite');?>"><?php echo get_option('telephonesite');?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-light bg-nav-menu">
            <div class="container">
                <a class="navbar-brand" href="<?php bloginfo("url") ?>">
                    <?php
                    if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
                    }
                    ?>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-header" aria-controls="menu-header" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="menu-header">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'navbar-nav menu-header ml-auto')); ?>
                </div>
            </div>
        </nav>
        <div class="social-share">
            <?php echo do_shortcode('[Sassy_Social_Share]'); ?>
        </div>
    </header>