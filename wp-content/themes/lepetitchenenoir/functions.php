<?php
register_nav_menus(array('primary' => __('Primary Menu','Header')));
add_theme_support( 'post-thumbnails');
add_filter ( 'nav_menu_css_class', 'so_37823371_menu_item_class', 10, 4 );
add_theme_support( 'custom-logo' );
function widget_contact() {

    register_sidebar( array(
        'name'          => 'Coordonnée',
        'id'            => 'coordonnee',
        'description' => '',
        'before_widget' => '<div class="coordonne">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
}
add_action( 'widgets_init', 'widget_contact' );

function so_37823371_menu_item_class ( $classes, $item, $args ){
    if($args->theme_location == 'primary') {
        $classes[] = 'nav-item';
    }
    return $classes;
}
add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    if($args->theme_location == 'primary') {
        $class = 'nav-link';
        $atts['class'] = $class;
    }
    return $atts;
}

add_action('admin_menu','lepetitchenenoir_theme_option');
function lepetitchenenoir_theme_option()
{
    add_menu_page("Petitchenenoir theme options","Options theme","activate_plugins","option_primaire","render_option_html",null);
}
function render_option_html()
{
    if(isset($_POST["saveoptions"])):
        if(wp_verify_nonce( $_POST['_wpnoncesite'], 'lepetitchenenoirthemeoption' )):
            foreach($_POST['options'] as $key=>$value):
                if(!empty($value))
                    update_option($key,$value);
                else
                    delete_option($key);
            endforeach;
            ?>
            <div id="message" class="updated notice notice-success is-dismissible">
                <p>Options sauvegardées avec success</p>
            </div>
        <?php
        else:
        endif;
    endif;
    ?>
    <div class="wrap theme-options-page">
        <h1>Option du theme</h1>
        <form action="" method="post">
            <div class="theme-options-group">
                <table class="form-table">
                    <tr><th colspan="2"><h2>Option dans le header</h2></th></tr>
                    <tr>
                        <th>
                            <label for="telephonesite">Téléphone du site:</label>
                        </th>
                        <th>
                            <input type="text" name="options[telephonesite]" id="telephonesite" value="<?php echo get_option('telephonesite');?>"></input>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <label for="emailsite">Adresse e-mail du site:</label>
                        </th>
                        <th>
                            <input type="text" name="options[emailsite]" id="emailsite" value="<?php echo get_option('emailsite');?>"></input>
                        </th>
                    </tr>
                    <tr><th colspan="2"><h2>Réseaux sociaux</h2></th></tr>
                    <tr>
                        <th>
                            <label for="fbsite">Adresse facebook du site:</label>
                        </th>
                        <th>
                            <input type="text" name="options[fbsite]" id="fbsite" value="<?php echo get_option('fbsite');?>">
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <label for="linkedinsite">Adresse linkedin du site:</label>
                        </th>
                        <th>
                            <input type="text" name="options[linkedinsite]" id="linkedinsite" value="<?php echo get_option('linkedinsite');?>">
                        </th>
                    </tr>
                    <tr><th colspan="2"><h2>Option dans le footer</h2></th></tr>
                    <tr>
                        <th>
                            <label for="adressesite">Copyright:</label>
                        </th>
                        <th>
                            <textarea name="options[adressesite]" id="adressesite" value="<?php echo get_option('adressesite');?>" col="20" rows="5"><?php echo get_option('adressesite');?></textarea>
                        </th>
                    </tr>
                </table>
            </div>
            <input type="hidden" name="_wpnoncesite" id="_wpnoncesite" value="<?php echo wp_create_nonce('lepetitchenenoirthemeoption');?>"/>
            <input type="submit" name="saveoptions" id="saveoptions" value="Enregistrer" class="button button-primary"/>
        </form>
    </div>
    <?php
}

/* Partenaire */
function partenaire() {
    $labels = array(

        'name'                => _x( 'Partenaire', 'Post Type General Name', 'text_domain' ),

        'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name', 'text_domain' ),

        'menu_name'           => __( 'Partenaire', 'text_domain' ),

        'all_items'           => __( 'Tous les partenaires', 'text_domain' ),

        'view_item'           => __( 'Voir', 'text_domain' ),

        'add_new_item'        => __( 'Ajouter un nouveau partenaire', 'text_domain' ),

        'add_new'             => __( 'Ajouter un nouveau partenaire', 'text_domain' ),

        'edit_item'           => __( 'Modifier', 'text_domain' ),

        'update_item'         => __( 'Mettre a jour', 'text_domain' ),

        'search_items'        => __( 'Rechercher un partenaire', 'text_domain' ),

        'not_found'           => __( 'Aucun partenaire trouver', 'text_domain' ),

        'not_found_in_trash'  => __( 'Aucun partenaire trouver', 'text_domain' ),

    );

    $args = array(

        'label'               => __( 'Nos partenaire', 'text_domain' ),

        'description'         => __( 'Nos partenaires information pages', 'text_domain' ),

        'labels'              => $labels,

        'supports'            =>   array( 'title', 'thumbnail'),

        'taxonomies'          => array(),

        'hierarchical'        => false,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'menu_position'       => '',

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => true,

        'publicly_queryable'  => true,

        'capability_type'     => 'post',

    );

    register_post_type( 'partenaire', $args );

}
add_action( 'init', 'partenaire' );

/* Service */
function service() {
    $labels = array(

        'name'                => _x( 'Service', 'Post Type General Name', 'text_domain' ),

        'singular_name'       => _x( 'Service', 'Post Type Singular Name', 'text_domain' ),

        'menu_name'           => __( 'Service', 'text_domain' ),

        'all_items'           => __( 'Tous les services', 'text_domain' ),

        'view_item'           => __( 'Voir', 'text_domain' ),

        'add_new_item'        => __( 'Ajouter un nouveau service', 'text_domain' ),

        'add_new'             => __( 'Ajouter un nouveau service', 'text_domain' ),

        'edit_item'           => __( 'Modifier', 'text_domain' ),

        'update_item'         => __( 'Mettre a jour', 'text_domain' ),

        'search_items'        => __( 'Rechercher un service', 'text_domain' ),

        'not_found'           => __( 'Aucun service trouver', 'text_domain' ),

        'not_found_in_trash'  => __( 'Aucun service trouver', 'text_domain' ),

    );

    $args = array(

        'label'               => __( 'Nos services', 'text_domain' ),

        'description'         => __( 'Nos services information pages', 'text_domain' ),

        'labels'              => $labels,

        'supports'            =>   array( 'title', 'thumbnail', 'editor'),

        'taxonomies'          => array(),

        'hierarchical'        => false,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'show_in_nav_menus'   => true,

        'show_in_admin_bar'   => true,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => true,

        'publicly_queryable'  => true,

        'capability_type'     => 'post',

    );

    register_post_type( 'service', $args );

}
add_action( 'init', 'service' );