<footer id="footer" class="footer">
    <div class="container">
        <div class="menu-footer text-center">
            <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'list-menu-footer')); ?>
        </div>
        <div class="info-footer text-center">
            <p>
                <?php echo get_option('adressesite');?>
                <a target="_blank" href="<?php echo get_option('linkedinsite');?>">
                    <?php echo ("Zone46");?>
                </a>
            </p>
        </div>
    </div>
</footer>
</div>
<!--<script type="text/javascript" src="--><?php //bloginfo("template_directory");?><!--/js/jquery-3.3.1.min.js"></script>-->
<script type="text/javascript" src="<?php bloginfo("template_directory");?>/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory");?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory");?>/js/popper.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory");?>/js/slick.min.js"></script>
<script type="text/javascript" src="<?php bloginfo("template_directory");?>/js/custom.js"></script>
<?php wp_footer(); ?>
</body> 
</html> 
