<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header("404"); ?>

<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>:(</h1>
        </div>
        <h2><?php echo ("404 - Page non trouvée");?></h2>
        <p>La page que vous recherchez a peut-être été supprimée si son nom a été modifié ou est temporairement indisponible.</p>
        <a href="<?php bloginfo("url");?>"><?php echo ("Accueil");?></a>
    </div>
</div>
