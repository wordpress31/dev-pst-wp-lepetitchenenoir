<!DOCTYPE html>
<html class=""<?php language_attributes(); ?>>
<head>
    <title><?php wp_title('|',true,'right'); bloginfo('name');?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/css/404.css" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Passion+One:900" rel="stylesheet">
    <link rel="icon" href="<?php bloginfo( 'template_directory' ); ?>/favicon.ico" />
    <?php wp_head(); ?>
</head>
