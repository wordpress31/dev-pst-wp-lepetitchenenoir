$(document).ready(function(){
    $('.list-logo-talk-about-us').slick({
        slidesToShow: 3,
        arrows:false,
        centerPadding:"60px",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
});
    $(document).on("scroll", onScroll);
    $('.menu-header a').on('click', function(evt){
        evt.preventDefault();
        var target = $(this).attr('href');
        $('html, body')
            .stop()
            .animate({scrollTop: $(target).offset().top-70}, 1000 );
    });
    function onScroll(event){
        var scrollPosition = $(document).scrollTop();
        $('.menu-header a').each(function () {
            var currentLink = $(this);
            var refElement = $(currentLink.attr("href"));
        });
    }

    $(document).on("scroll", onScroll);
    $('.list-menu-footer a, .link-contact').on('click', function(evt){
        evt.preventDefault();
        var target = $(this).attr('href');
        $('html, body')
            .stop()
            .animate({scrollTop: $(target).offset().top-70}, 1000 );
    });
    function onScroll(event){
        var scrollPosition = $(document).scrollTop();
        $('.list-menu-footer a, .link-contact').each(function () {
            var currentLink = $(this);
            var refElement = $(currentLink.attr("href"));
        });
    }
});
