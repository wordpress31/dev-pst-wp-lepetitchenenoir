<?php
/*
Template Name: Home
*/
?>

<?php get_header();?>
<section class="banner-video" id="home">
    <!--<video autoplay muted loop>-->
    <!--<source src="images/vidéodaddy.mp4" type="video/mp4">-->
    <!--<source src="images/vidéodaddy.ogg" type="video/ogg">-->
    <!--</video>-->
    <img src="<?php bloginfo( 'template_directory' ); ?>/images/video-default.png" class="img-fluid" alt="">
    <div class="container">
        <div class="text-banner">
            <?php echo get_field('text_banner'); ?>
        </div>
    </div>
</section>
<section class="about" id="about">
    <div class="container">
        <div class="title-section text-center">
            <h1><?php echo("A propos");?></h1>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="img-about">
                    <img src="<?php echo get_field('img_about'); ?>" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-about">
                    <?php echo get_field('content_about'); ?>
                </div>
                <div class="btn-about">
                    <ul>
                        <li>
                            <a href="tel:<?php echo get_option('telephonesite');?>">
                                <?php echo ("Appelez-nous");?>
                            </a>
                        </li>
                        <li>
                            <a class="link-contact" href="#contact">
                                <?php echo ("Ecrivez-nous");?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="other-content-about">
            <div class="row">
                <div class="col-md-12">
                    <div class="other-title-about">
                        <h3>
                            <?php echo get_field('title_about'); ?>
                        </h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="other-text-about">
                        <?php echo get_field('content_2about'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="other-img-about">
                        <img src="<?php echo get_field('img_about2'); ?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="other-img-about">
                        <img src="<?php echo get_field('img_about3'); ?>" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="talk-about-us">
            <h4 class="text-center"><?php echo ("Ils parlent de nous");?></h4>
            <div class="list-logo-talk-about-us">
                <?php
                $_partenaire= new WP_Query(array(
                    'post_type' => 'partenaire',
                    'posts_per_page' => '-1',
                    'order'=>'ASC',
                ));
                while($_partenaire->have_posts()):
                    $_partenaire->the_post();
                 ?>
                <div class="logo-talk-about-us">
                    <a href="<?php echo get_field('link_article'); ?>" target="_blank">
                        <?php
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $_partenaire->ID, 'full' ), 'single-post-thumbnail' );
                        ?>
                        <img src="<?php echo $image[0];?>" class="img-fluid" alt="">
                    </a>
                </div>
                <?php endwhile;
                     wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>
<section class="service" id="service">
    <div class="container">
        <div class="title-section text-center">
            <h1><?php echo ("Nos services");?></h1>
        </div>
        <div class="row">
            <?php
            $_service= new WP_Query(array(
                'post_type' => 'service',
                'posts_per_page' => '-1',
                'order'=>'ASC',
            ));
            while($_service->have_posts()):
            $_service->the_post();
            ?>
            <div class="col-md-6">
                <div class="content-service">
                    <?php
                    $img_service = wp_get_attachment_image_src( get_post_thumbnail_id( $_service->ID, 'full' ), 'single-post-thumbnail' );
                    ?>
                    <img src="<?php echo $img_service[0];?>" class="img-fluid" alt="">
                    <p>
                        <?php echo get_the_content();?>
                    </p>
                </div>
            </div>
            <?php endwhile;
            wp_reset_postdata();
            ?>
        </div>
        <div class="btn-phone-service">
            <a href="tel:<?php echo get_option('telephonesite');?>">
                <?php echo ("Appelez-nous");?>
            </a>
        </div>
        <div class="bloc-galerie">
            <div class="row">
                <div class="col-md-4">
                    <div class="galerie-photo-list">
                        <?php echo do_shortcode('[envira-gallery id="92"]'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="video-site">
                        <video autoplay muted loop>
                            <source src="<?php bloginfo( 'template_directory' ); ?>/images/vidéodaddy.mp4" type="video/mp4">
                            <source src="<?php bloginfo( 'template_directory' ); ?>/images/vidéodaddy.ogg" type="video/ogg">
                        </video>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img-site">
                        <img src="<?php echo get_field('image_section_galerie'); ?>" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact" id="contact">
    <div class="container">
        <div class="title-section text-center">
            <h1><?php echo ("Contact");?></h1>
        </div>
        <div class="row">
            <div class="col-md-5">
                <?php dynamic_sidebar('coordonnee');?>
            </div>
            <div class="col-md-7">
                <div class="form-contact">
                    <?php echo do_shortcode('[contact-form-7 id="81" title="Contact"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>

